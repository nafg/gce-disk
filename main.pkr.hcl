source "googlecompute" "main" {
  disk_type           = "pd-ssd"
  image_family        = "ubuntu-nafg"
  image_name          = "ubuntu-nafg-${formatdate("YYYY-MM-DD", timestamp())}"
  project_id          = "gitlab-runner-189405"
  source_image_family = "ubuntu-2204-lts"
  ssh_username        = "ubuntu"
  zone                = "us-east1-b"
}

build {
  sources = ["source.googlecompute.main"]

  provisioner "shell" {
    script = "${path.root}/provision.sh"
  }
}

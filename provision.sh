#!/bin/bash -xe


# Override apt-get with a version that waits for the lock
curl https://gist.githubusercontent.com/nafg/a8be82385742233410aaa072bbc5ef76/raw/83c561044edb422777839d49d07121db8e53469a/apt-get | \
    sudo install -T /dev/stdin /usr/local/bin/apt-get

export DEBIAN_FRONTEND=noninteractive

# Keep packages up to date
sudo apt-get update
sudo -E apt-get upgrade -y
sudo -E apt-get autoremove -y

# Install docker
sudo -E apt-get install -y apt-transport-https ca-certificates curl gnupg software-properties-common lsb-release
gpg_file=/usr/share/keyrings/docker-archive-keyring.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o "$gpg_file"

echo \
  "deb [arch=amd64 signed-by=$gpg_file] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
  | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo -E apt-get install -y docker-ce docker-ce-cli containerd.io jq moreutils

docker_daemon_file=/etc/docker/daemon.json
if [ ! -f $docker_daemon_file ]; then
  echo "{}" | sudo tee $docker_daemon_file
fi
jq 'setpath(["registry-mirrors"]; .["registry-mirrors"] + ["https://mirror.gcr.io"])' $docker_daemon_file | sudo sponge $docker_daemon_file

# Install docker-compose
sudo -E apt-get install -y python3-pip
sudo -H pip install --upgrade pip docker-compose

# Install gcsfuse
GCSFUSE_REPO=gcsfuse-$(lsb_release -cs)
echo "deb [signed-by=/usr/share/keyrings/cloud.google.asc] https://packages.cloud.google.com/apt $GCSFUSE_REPO main" | sudo tee /etc/apt/sources.list.d/gcsfuse.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo tee /usr/share/keyrings/cloud.google.asc
sudo -E apt-get update
sudo -E apt-get install -y gcsfuse || echo Installing gcsfuse failed

# Install Ops agent (logging and monitoring)
curl -sSO https://dl.google.com/cloudagents/add-google-cloud-ops-agent-repo.sh
sudo bash add-google-cloud-ops-agent-repo.sh --also-install

# Install Scala tools
curl -fL https://github.com/coursier/launchers/raw/master/cs-x86_64-pc-linux.gz | gzip -d > cs
chmod +x cs
./cs setup --yes
rm -f cs
